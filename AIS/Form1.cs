﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace AIS
{
    public partial class FormMain : Form
    {
        #region Variables
        private MySqlConnection mySqlConnection;
        private MySqlCommand mySqlCommand;
        #endregion

        #region MyMethods
        private void RefreshDataGridVieOrders()
        {

        }
        #endregion

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            string connectionString = "Server=127.0.0.1;Port=3306;User=root;Password=1234;Database=ais_products_togo";

            mySqlConnection = new MySqlConnection(connectionString);
            mySqlConnection.Open();

            mySqlCommand = new MySqlCommand();
            mySqlCommand.Connection = mySqlConnection;
        }

        private void FormMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            mySqlConnection.Close();
        }
    }
}
